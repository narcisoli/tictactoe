import { useDispatch } from "react-redux"
import { Action, bindActionCreators } from "redux"

export const useActions = <T extends { [a: string]: any }, S extends (keyof T) & string>(actions: T, ...args: Array<S>): { [k in S]: T[k] } => {
    type Dispatch<A extends Action> = { <T extends A>(action: T): T }
    const dispatch = useDispatch<Dispatch<any>>()

    const res = (args.length ? args : Object.keys(actions)).reduce<{ [k in S]: T[k] }>((obj, key) => ({
        ...obj, [key]: bindActionCreators(actions[key], dispatch)
    }), {} as { [k in S]: T[k] })
    return res as typeof args extends undefined ? T : { [k in S]: T[k] }
}