import axios from "axios"
import { IResult } from "../models"

// const url = "http://localhost:3033/results"
const url = "https://tctactoewsnarcis.herokuapp.com/api/results"

export const getResults = () =>
    new Promise<IResult[]>((resolve, reject) => {
        axios.get(url).then(
            res => resolve(res.data?.reverse() ?? []),
            reject
        )
    })


export const addResult = (res: IResult) =>

    new Promise<IResult[]>((resolve, reject) => {
        axios.post(url + "/add", res).then(
            res => resolve(res.data),
            reject
        )
    })
