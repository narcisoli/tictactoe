export function classes(...classes: (string | false | undefined | null | { [className: string]: any })[]): string {
    return classes.map(c => c && typeof c === 'object' ? Object.keys(c).map(key => !!c[key] && key) : [c])
        .reduce((flattened, c) => flattened.concat(c), [] as string[]).filter(c => !!c).join(' ')
}


export function bem(...classes: (string | false | undefined | null | { [className: string]: any })[]): string {
    const classes1D = classes.map(c => c && typeof c === 'object' ? Object.keys(c).map(key => !!c[key] && key) : [c])
        .reduce((flattened, c) => flattened.concat(c), [] as string[])
    if (classes1D.length < 2) return classes1D?.[0] || ""
    return classes1D.filter(c => c).map((x, idx) => idx && `${classes1D[0]}--${x}` || x).join(" ")
}
