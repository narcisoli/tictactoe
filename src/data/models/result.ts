export type IResult = {
    player1: string
    player2: string
    result: number
}