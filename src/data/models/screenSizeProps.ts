
export type ScreenSizeProps = {
    screenSize: "xs" | "s" | "m" | "l" | "xl"
}

