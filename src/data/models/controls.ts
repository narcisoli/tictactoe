

import { Action } from "redux"

export interface AsyncAction<A = Action, S = {}, R = void> {
    (dispatch: ActionDispatch<A, S, R>, getState: () => S): R
}
export interface ActionDispatch<A = Action, S = {}, R = void> {
    (action: A): A
    (action: AsyncAction<A, S, R>): Promise<void>
}





export type Sizes = "xl" | "l" | "m" | "s" | "xs"

export type Skins = "primary" | "secondary" | "highlight" | "danger" | "success" | "warning"

export type ButtonLayouts = "none" | "rounded" | "ghost" | "shaded" | "holo" | "bordered" | "iconRight" | "circle" | "dropshadow"
