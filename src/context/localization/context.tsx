import * as React from "react"
import { ILocalization, ITranslations, Language } from "./model"

const na = "Not available"
export const LocalizationContext = React.createContext<ILocalization>({
    changeLanguage: (x: string) => na,
    language: "ro",
    translateText: (x: string | number, language?: Language) => na,
    languages: ["ro"],
    config: { languages: {}, translations: {} }
})

type Props = {
    config: ITranslations
    children: React.ReactNode
}

export const LocalizationProvider: React.FC<Props> = ({ config, children }) => {
    const [language, setLanguage] = React.useState<Language>(localStorage.getItem("lang") as Language ?? "ro")
    const languages = Object.keys(config.languages).filter(key => config.languages[key])

    const changeLanguage = (lang: Language) => {
        localStorage.setItem("lang", lang)
        setLanguage(lang)
    }
    const translate = (id: string | number, customlanguage?: Language) => {

        return (config?.translations?.[id.toString().toLowerCase()] as any)?.[customlanguage ?? language] || `##${id.toString().toLowerCase()}`
    }


    return (

        <LocalizationContext.Provider value={{ config, languages, language, translateText: translate, changeLanguage }}>
            {children}
        </LocalizationContext.Provider>
    )
}
