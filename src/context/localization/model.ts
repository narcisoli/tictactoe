export type ILocalization = {
    translateText: (id: string | number, language?: Language) => string
    changeLanguage: (language: Language) => void
    language: Language
    languages: string[]
    config: ITranslations
}
export type Language = "ro" | "en"

export type Translation = Record<Language, string>

export type ITranslations = {
    languages: Record<string, boolean>
    translations: Record<string, Record<Language, string>>
}