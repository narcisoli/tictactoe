import { IResult } from "../data/models/result"


export type IStore = {
    player1?: string
    player2?: string
    results: IResult[]
    resultsLoading?: boolean
    resultsError?: boolean
    startGame?: boolean

}

export type ComponentActionType =
    | { type: "SET_PLAYER1_NAME", payload: string }
    | { type: "SET_PLAYER2_NAME", payload: string }
    | { type: "RESULTS_LOADING" }
    | { type: "RESULTS_LOADED", payload: IResult[] }
    | { type: "RESULTS_ERROR" }
    | { type: "SET_START_GAME", payload?: boolean }
    | { type: "ADD_RESULT", payload: IResult }

const DEFAULT_STATE: IStore = {
    results: []
}


export const reducer = (state = DEFAULT_STATE, action: ComponentActionType): IStore => {
    switch (action.type) {
        case "SET_PLAYER1_NAME":
            return {
                ...state,
                player1: action.payload
            }
        case "SET_PLAYER2_NAME":
            return {
                ...state,
                player2: action.payload
            }

        case "RESULTS_LOADING": {
            return {
                ...state,
                resultsLoading: true,
                resultsError: false
            }
        }
        case "RESULTS_LOADED": {

            return {
                ...state,
                results: action.payload,
                resultsLoading: false
            }
        }
        case "RESULTS_ERROR": {
            return {
                ...state,
                resultsLoading: false,
                resultsError: true
            }
        }
        case "SET_START_GAME": {
            return {
                ...state,
                startGame: action.payload

            }
        }
        case "ADD_RESULT": {
            return {
                ...state,
                results: [
                    action.payload,
                    ...state.results
                ]
            }
        }
    }
    return state
}