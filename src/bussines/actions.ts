
import { AsyncAction, IResult } from "../data/models"
import { getResults } from "../data/repos/results"
import { ComponentActionType, IStore } from "./reducer"

const loadResults = (): AsyncAction<ComponentActionType, IStore> => (dispatch, getState) => {

    dispatch({ type: "RESULTS_LOADING" })
    getResults().then(results => {

        dispatch({ type: "RESULTS_LOADED", payload: results })
    }, _ => dispatch({ type: "RESULTS_ERROR" }))
}

const setPlayer1 = (name: string): ComponentActionType =>
    ({ type: "SET_PLAYER1_NAME", payload: name })

const setPlayer2 = (name: string): ComponentActionType =>
    ({ type: "SET_PLAYER2_NAME", payload: name })

const setStartGame = (val: boolean): ComponentActionType =>
    ({ type: "SET_START_GAME", payload: val })

const addResult = (result: IResult): ComponentActionType =>
    ({ type: "ADD_RESULT", payload: result })

export const Actions = { loadResults, setPlayer1, setPlayer2, setStartGame, addResult }
