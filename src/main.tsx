import React from 'react'
import ReactDOM from 'react-dom'
import './styles.css'
import { Route, Router, Switch } from "react-router-dom"
import { createBrowserHistory } from "history"
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { SizeProvider } from './context/sizeProvider'
import Results from './components/results/component'
import Play from './components/play/component'
import { reducer } from './bussines/reducer'

import { LocalizationProvider } from "./context/localization"
import Navigation from './components/navigation/navigation'
import { loadSprites } from './data/helpers/sprites'
import { Actions } from './bussines/actions'

import translations from "./translations.json"

const store = createStore(reducer, applyMiddleware(thunk))

store.dispatch<any>(Actions.loadResults())

const renderContent = () => {

    return (
        <React.StrictMode>
            <Router history={createBrowserHistory()}>
                <LocalizationProvider config={translations} >
                    <Provider store={store}>
                        <SizeProvider>

                            <Route path="/:tab?">
                                <Navigation />
                            </Route>

                            <Switch>
                                <Route path="/results" component={Results}></Route>

                                <Route path="/" component={Play} />
                            </Switch>
                        </SizeProvider>
                    </Provider>
                </LocalizationProvider>
            </Router>
        </React.StrictMode >
    )
}

loadSprites("/sprites.svg")


ReactDOM.render(
    renderContent(),
    document.getElementById('root')
)


