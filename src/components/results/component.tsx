
import * as React from "react"
import { useDispatch, useSelector } from "react-redux"
import { Actions } from "../../bussines/actions"
import { IStore } from "../../bussines/reducer"
import { useSize } from "../../context/sizeProvider"
import { IResult } from "../../data/models"
import { Button, Table, Text } from "../_shared"

type Props = {}

const Results: React.FC<Props> = ({ }) => {



    const results = useSelector((s: IStore) => s.results)
    const resultsLoading = useSelector((s: IStore) => s.resultsLoading)
    const resultsError = useSelector((s: IStore) => s.resultsError)



    const renderName1 = (item: IResult) => <Text  strong={item.result == 1}>{item.player1}</Text>
    const renderName2 = (item: IResult) => <Text strong={item.result == 2}>{item.player2}</Text>
    const renderResult = (item: IResult) => <Text>{item.result}</Text>

    if(resultsError){
        return <Text skin="danger"> ERORR </Text>
    }

    return (
        <div>

            <Table items={results} loading={resultsLoading}
                headers={["Player1", "Player2", "Result"]}
                columns={[renderName1, renderName2, renderResult]}
            />


        </div>
    )
}


export default Results