
import * as React from "react"
import { useSelector } from "react-redux"
import { IStore } from "../../bussines/reducer"
import { addResult } from "../../data/repos/results"
import { WinningPos } from "../../data/statics/winningPos"
import { Button, Text } from "../_shared"
import InputView from "./inputView/component"
import LastMoves from "./lastMoves/component"
import styles from "./component.module.css"
import Piece from "./piece/component"
import { useActions } from "../../data/hooks/useActions"
import { Actions } from "../../bussines/actions"
import { IResult } from "../../data/models"
import { useLocalization } from "../../context/localization"


type Props = {}

export type IValue = "x" | "y" | undefined
const initState: IValue[] = Array.from({ length: 9 })


const Play: React.FC<Props> = ({ }) => {
    const [values, setValues] = React.useState(initState)
    const [turn, setTurn] = React.useState<IValue>("x")
    const [winner, setwinner] = React.useState<IValue>()
    const [winnerPos, setWinnerPos] = React.useState<Number[]>([])
    const [moves, setMoves] = React.useState<[string, string][]>([])
    const actions = useActions(Actions, "addResult")

    const { translateText } = useLocalization()

    const player1 = useSelector((s: IStore) => s.player1)
    const player2 = useSelector((s: IStore) => s.player2)
    const startGame = useSelector((s: IStore) => s.startGame)

    const handleClick = async (idx: number) => {
        const vals = values.map((_x, idxx) => idxx == idx ? turn : _x)
        setValues(vals)

        const answer = checkWinner(turn, vals)
        if (answer?.length) {
            setWinnerPos(answer)
            setwinner(turn)

            const result: IResult = { player1: player1!, player2: player2!, result: turn == "x" ? 1 : 2 }
            await addResult(result)
            actions.addResult(result)
        }

        else {

            setTurn(turn == "x" ? "y" : "x")
            setMoves([...moves, [turn!, idx.toString()]])
        }
    }

    const checkWinner = (val: IValue, vals: IValue[]): number[] | undefined => {
        const wp = WinningPos.find(x => x.every(pos => vals[pos] == val))
        return wp
    }

    const handleUndo = (values: IValue[], turn: IValue, moves: [string, string][]) => {
        setValues(values)
        setTurn(turn)
        setMoves(moves)

    }

    const reset = () => {
        setwinner(undefined)
        setValues(initState)
        setTurn("x")
        setMoves([])
    }


    const renderGame = () => {

        return (
            <div className={styles.wrapper}>

                <div className={styles.board} >

                    {values.map((x, idx) =>
                        <Piece
                            key={idx}
                            onPressed={handleClick?.bind(undefined, idx)}
                            value={x}
                            className={styles.item}
                            disabled={!!winner && !winnerPos.includes(idx)}
                        />

                    )}
                    <Button onClick={reset} skin="danger" icon="refresh-ccw"></Button>
                </div>

                { !!moves.length && <LastMoves disabled={!!winner} moves={moves} onUndo={handleUndo} />}


            </div >
        )
    }



    if (startGame)
        return renderGame()



    return (
        <InputView />)
}

export default Play