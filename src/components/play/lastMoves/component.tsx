import * as React from "react"
import { classes } from "../../../data/helpers"
import { Button, Table, Text } from "../../_shared"
import { IValue } from "../component"
import styles from "./component.module.css"

type Props = {
    moves: [string, string][]
    disabled?: boolean
    onUndo: (values: IValue[], turn: IValue, moves: [string, string][]) => void
}

const LastMoves: React.FC<Props> = ({ moves, onUndo, disabled }) => {

    const handleUndo = (item: [string, string]) => {


        const idx = moves.indexOf(item)

        const newMoves = moves.splice(0, idx)

        let values: any[] = Array.from({ length: 9 })
        newMoves.forEach(([player, move]) => values[move as any] = player)


        const turn = newMoves[newMoves.length - 1]?.[0] == "x" ? "y" : "x"

        onUndo?.(values, turn, newMoves)

    }

    const renderName = (item: [string, string]) => <Text>{item?.[0]}</Text>
    const renderMove = (item: [string, string]) => <Text>{item?.[1]}</Text>
    const renderActions = (item: [string, string]) => <Button onClick={() => handleUndo(item)} icon={"arrow-left"}>UNDO</Button>

    return (


        <Table items={moves} className={classes(disabled && styles.disabled)}
            headers={["Player", "Move", "Actions"]}
            columns={[renderName, renderMove, renderActions]}
        />


    )
}


export default LastMoves