import * as React from "react"
import { Button, Icon, Textfield } from "../../_shared"
import styles from "./component.module.css"
import { useActions } from "../../../data/hooks/useActions"
import { Actions } from "../../../bussines/actions"
import { useSelector } from "react-redux"
import { IStore } from "../../../bussines/reducer"
import { useLocalization } from "../../../context/localization"

type Props = {}

const InputView: React.FC<Props> = ({ }) => {
    const actions = useActions(Actions, "setPlayer1", "setPlayer2", "setStartGame")


    const player1 = useSelector((s: IStore) => s.player1)
    const player2 = useSelector((s: IStore) => s.player2)
    const { translateText } = useLocalization()

    const handleStart = () => {
        if (player1 && player2)
            actions.setStartGame(true)

        else
            alert("Error")
    }

    return (
        <div className={styles.inputs}>

            <div className={styles.w1}>
                <Textfield onChange={actions.setPlayer1} placeholder={translateText("player1")} value={player1}></Textfield>
                <Textfield onChange={actions.setPlayer2} placeholder={translateText("player2")} value={player2}></Textfield>
            </div>
            <div className={styles.w2}>
                <Button
                    className={styles.btn}
                    onClick={handleStart}
                    // isActive={url.includes("/play")}
                    rounded> <Icon name="play"></Icon>{translateText("play")}</Button>
            </div>
        </div>)
}


export default InputView