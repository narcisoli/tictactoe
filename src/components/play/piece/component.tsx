import * as React from "react"
import { Skins } from "../../../data/models"
import { Button } from "../../_shared"
import { IValue } from "../component"

type Props = {
    disabled?: boolean
    onPressed: () => void
    className?: string
    value: IValue
}

const Piece: React.FC<Props> = ({ onPressed, value, children, className, disabled }) => {

    let skin: Skins | undefined = undefined
    if (value == "x")
        skin = "primary"
    if (value == "y")
        skin = "highlight"

    return <Button disabled={disabled} onClick={!value && onPressed || undefined}
        className={className}
        fakeButton
        skin={skin}>
        {value ?? ""}
    </Button >


}


export default Piece