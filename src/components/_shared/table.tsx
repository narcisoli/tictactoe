
import { em } from "csx"
import * as React from "react"
import { bem, classes } from "../../data/helpers"

import tinycolor from "tinycolor2"

import theme from "../../theme.json"
import { stylesheet } from "typestyle"
import { Loader, Text } from "."


type Props<T> = {
    className?: string
    headers: string[]
    columns: Array<(arg: T) => React.ReactElement>
    items: T[]
    loading?: boolean
    itemClassName?: string
}


const Table: React.FC<Props<any>> = ({ loading, columns, headers, items, itemClassName, className }) => {
    if (loading) {
        return (
            <div className={classes(className, bem(style.wrapper, loading && "loading"))}>
                <Loader />
            </div>
        )
    }
    return (
        <div className={classes(className, style.wrapper)}>
            <div className={style.header}>
                {headers.map((header, idx) =>
                    <Text className={style.text} skin="primary" key={idx} size="l" strong >
                        {header}
                    </Text>
                )}
            </div>
            <div className={style.content}>
                {items.map((item, idx) =>
                    <div key={idx} className={classes(style.item, itemClassName)}>
                        {columns.map((columnCallback, idx) =>
                            <div key={idx} className={style.cell}>
                                {columnCallback(item)}
                            </div>
                        )}
                    </div>
                )}
            </div>
        </div>
    )
}

const style = stylesheet({
    wrapper: {
        display: "flex",
        flexDirection: "column",
        borderRadius: theme.radius.rounded
    },
    header: {
        display: "flex",
        textTransform: "uppercase",
        backgroundColor: theme.colors.default,
    },
    text: {
        textAlign: "center",
        padding: theme.margin.s,
        flex: 1,
    },
    content: {
        flex: 1,
        display: "flex",
        flexDirection: "column",
        $nest: {
            "&--loading": {
                justifyContent: "center",
                alignItems: "center"
            }
        }
    },
    item: {
        display: "flex",
        backgroundColor: tinycolor(theme.colors.default).lighten(30).toString(),
        $nest: {
            "&:nth-of-type(even)": {
                backgroundColor: theme.colors.default
            },
            "&:hover": {
                backgroundColor: tinycolor(theme.colors.primary).lighten(30).toString(),
            }
        }
    },
    cell: {
        display: "flex",
        justifyContent: "center",
        padding: theme.margin.s,
        flex: 1,
        minWidth: em(7)
    }

})

export default Table