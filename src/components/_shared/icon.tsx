import * as React from "react"
import { classes, style } from "typestyle"
import { bem } from "../../data/helpers"
import { Sizes } from "../../data/models"
import theme from "../../theme.json"


type Props = {
    className?: string
    name?: string
    size?: Sizes
}



const Icon: React.FC<Props> = ({ className, size, name }) => {
    const href = `#${name}`

    return (
        <svg className={classes(bem(iconStyle, size && `size--${size}`), className)}>
            <use xlinkHref={href}></use>
        </svg>

    )
}



const iconStyle = style({
        width: theme.iconSizes.m,
        height: theme.iconSizes.m,
        opacity: theme.opacity.secondary,
        fontFamily: theme.fontFamily.materialIcons,
        $nest: {
            "&--size": {
                $nest: {
                    "&--xs": {
                        width: theme.iconSizes.xs,
                        height: theme.iconSizes.xs,
                        fontSize: theme.iconSizes.xs
                    },
                    "&--s": {
                        width: theme.iconSizes.s,
                        height: theme.iconSizes.s,
                        fontSize: theme.iconSizes.s
                    },
                    "&--m": {
                        width: theme.iconSizes.m,
                        height: theme.iconSizes.m,
                        fontSize: theme.iconSizes.m
                    },
                    "&--l": {
                        width: theme.iconSizes.l,
                        height: theme.iconSizes.l,
                        fontSize: theme.iconSizes.l
                    },
                    "&--xl": {
                        width: theme.iconSizes.xl,
                        height: theme.iconSizes.xl,
                        fontSize: theme.iconSizes.xl
                    }
                }
            },
        }
    }
)




export default Icon
