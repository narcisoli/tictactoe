import * as React from "react"
import classes from "./textfield.module.css"

type Props = {
    onChange?: (val: string) => void
    value?: string
    placeholder?: string

}

const Textfield: React.FC<Props> = ({ onChange, value, placeholder }) => {
    return (
        <input placeholder={placeholder} className={classes.input} onChange={e => onChange?.(e.target.value)} value={value ?? ""}></input>
    )
}


export default Textfield