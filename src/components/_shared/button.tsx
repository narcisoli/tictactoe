import { padding } from "csx"
import * as React from "react"
import { classes, style } from "typestyle"
import { bem } from "../../data/helpers"
import { ButtonLayouts, Sizes, Skins } from "../../data/models"


import theme from "../../theme.json"
import Icon from "./icon"


type Props = {
    size?: Sizes
    icon?: string
    layout?: Array<ButtonLayouts>
    className?: string
    id?: string
    disabled?: boolean
    isActive?: boolean
    skin?: Skins
    rounded?: boolean
    uppercase?: boolean
    onClick?(action: any): void
    fakeButton?: boolean
    style?: React.CSSProperties
    ref?: React.Ref<any>
}

export type ButtonLayout = "none" | "rounded" | "ghost" | "shaded" | "holo" | "bordered" | "iconRight" | "circle" | "dropshadow"
export type ButtonSkins = "warning" | "danger" | "success" | "highlight" | "primary"

const Button: React.FC<Props> = ({ children, id, className, onClick, size = "m", isActive, icon, style: cssStyle, layout, disabled, fakeButton, skin, rounded, uppercase }) => {

    const ownClassName = bem({
        [btnStyle]: true,
        disabled,
        rounded,
        uppercase,
        active: isActive,
        ghost: layout?.includes("ghost"),
        [`size--${size}`]: size,
        [`skin--${skin}`]: skin,
    })

    const clsName = classes(ownClassName, className)

    if (fakeButton)
        return (
            <div
                className={clsName}
                id={id}
                onClick={onClick}
                style={cssStyle}
            >
                {icon && <Icon className="icon" name={icon} />}
                {children}
            </div>
        )

    return (
        <button
            disabled={!!disabled}
            onClick={onClick}
            className={clsName}
            id={id}
            style={cssStyle}
        >
            {icon && <Icon className={"icon"} name={icon} />}
            {children}
        </button>
    )

}


const btnStyle = style({
    $debugName: "btn",
    fontSize: theme.textSizes.m,
    fontFamily: theme.fontFamily.primary,
    opacity: theme.opacity.primary,
    color: theme.fitColors.default,
    backgroundColor: theme.colors.default, //todo default
    borderRadius: theme.radius.default,
    border: "1px solid transparent",
    cursor: "pointer",
    outline: "none",
    display: "inline-block",
    padding: theme.buttonPaddings.m,
    textDecoration: "none",
    boxShadow: theme.shadows.material,
    $nest: {
        "&--size": {
            $nest: {
                "&--xs": {
                    fontSize: theme.textSizes.xs,
                    padding: theme.buttonPaddings.xs,
                },
                "&--s": {
                    fontSize: theme.textSizes.s,
                    padding: theme.buttonPaddings.s,
                },
                "&--m": {
                    fontSize: theme.textSizes.m,
                    padding: theme.buttonPaddings.m,
                },
                "&--l": {
                    fontSize: theme.textSizes.l,
                    padding: theme.buttonPaddings.l,
                },
                "&--xl": {
                    fontSize: theme.textSizes.xl,
                    padding: theme.buttonPaddings.xl,
                }
            }
        },
        "&--skin": {
            $nest: {
                "&--default": {
                    backgroundColor: theme.colors.default,
                    color: theme.fitColors.default
                },
                "&--danger": {
                    backgroundColor: theme.colors.danger,
                    color: theme.fitColors.danger
                },
                "&--highlight": {
                    backgroundColor: theme.colors.highlight,
                    color: theme.fitColors.highlight
                },
                "&--primary": {
                    backgroundColor: theme.colors.primary,
                    color: theme.fitColors.primary
                },
                "&--secondary": {
                    backgroundColor: theme.colors.secondary,
                    color: theme.fitColors.secondary
                },
                "&--success": {
                    backgroundColor: theme.colors.success,
                    color: theme.fitColors.success
                },
                "&--warning": {
                    backgroundColor: theme.colors.warning,
                    color: theme.fitColors.warning
                }
            }
        },
        "&--ghost": {
            backgroundColor: "transparent"
        },
        "&--rounded": {
            borderRadius: theme.radius.rounded, padding: padding(theme.margin.m, theme.margin.m)
        },
        "&--uppercase": {
            textTransform: "uppercase"
        },
        "&--active": {
            backgroundColor: theme.colors.highlight, color: theme.fitColors.highlight
        },
        "&--disabled": {
            cursor: "default",
            pointerEvents: "none",
            opacity: theme.opacity.disabled
        },
        ".icon": {
            marginRight: theme.margin.m
        }
    }
})

export default Button
