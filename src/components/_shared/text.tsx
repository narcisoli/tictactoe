import * as React from "react"
import { classes, style } from "typestyle"
import { bem } from "../../data/helpers"
import { Sizes, Skins } from "../../data/models"
import theme from "../../theme.json"



type Props = {
    className?: string
    size?: Sizes
    fontFamily?: "primary" | "secondary" | "serif"
    skin?: Skins
    strong?: boolean
    sub?: boolean
    block?: boolean,
    succes?: boolean
    disabled?: boolean
}

const Text: React.FC<Props> = ({ className, children, disabled, fontFamily, block, strong, sub, skin, size, }) => {


    const ownClassName = bem({
        [ss]: true,
        disabled,
        strong,
        sub,
        block,
        [`size--${size}`]: size,
        [`fontFamily--${fontFamily}`]: fontFamily,
        [`skin--${skin}`]: skin,
    })
    return (
        <div className={classes(ownClassName, className)}>
            {children}
        </ div>
    )
}


const { opacity, fontFamily, textSizes, fontWeight } = theme
const ss = style({
    opacity: opacity.primary,
    fontSize: textSizes.m,
    fontFamily: fontFamily.primary,
    display: "inline",
    $nest: {
        "&--sub": {
            opacity: opacity.secondary
        },
        "&--fontFamily": {
            $nest: {
                "&--primary": {
                    fontFamily: theme.fontFamily.primary
                },
                "&--secondary": {
                    fontFamily: theme.fontFamily.secondary
                },
                "&--serif": {
                    fontFamily: theme.fontFamily.serif
                },
            }
        },
        "&--size": {
            $nest: {
                "&--xs": {
                    fontSize: theme.textSizes.xs,
                    padding: theme.buttonPaddings.xs,
                },
                "&--s": {
                    fontSize: theme.textSizes.s,
                    padding: theme.buttonPaddings.s,
                },
                "&--m": {
                    fontSize: theme.textSizes.m,
                    padding: theme.buttonPaddings.m,
                },
                "&--l": {
                    fontSize: theme.textSizes.l,
                    padding: theme.buttonPaddings.l,
                },
                "&--xl": {
                    fontSize: theme.textSizes.xl,
                    padding: theme.buttonPaddings.xl,
                }
            }
        },
        "&--strong": {
            fontWeight: fontWeight.bold
        },
        "&--block": {
            display: "block"
        },
        "&--skin": {
            $nest: {
                "&--default": {
                    color: theme.colors.default
                },
                "&--danger": {
                    color: theme.colors.danger
                },
                "&--highlight": {
                    color: theme.colors.highlight
                },
                "&--primary": {
                    color: theme.colors.primary
                },
                "&--secondary": {
                    color: theme.colors.secondary
                },
                "&--success": {
                    color: theme.colors.success
                },
                "&--warning": {
                    color: theme.colors.warning
                }
            }
        },
    }
})



export default Text
