import { border, deg, em, percent, px, rotate } from "csx"
import * as React from "react"
import { keyframes, stylesheet } from "typestyle"

import tinycolor from "tinycolor2"
import { classes } from "../../data/helpers"
import theme from "../../theme.json"


type Props = {
    className?: string
}

const Loader: React.FC<Props> = ({ className }) => {
    return (
        <div className={classes(style.base, className)} />
    )
}

const animation = keyframes({
    "0%": {
        transform: rotate(0)
    },
    "100%": {
        transform: rotate(deg(360))
    }
})


const style = stylesheet({
    base: {
        width: em(1),
        height: em(1),

        borderRadius: percent(50),
        border: border({ width: px(4), style: "solid", color: tinycolor(theme.colors.primary).lighten(20).toString() }),
        borderTopColor: theme.colors.primary,
        animation: `${animation}  1s linear infinite`
    }
})




export default Loader