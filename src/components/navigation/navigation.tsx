import * as React from "react"
import { useSelector } from "react-redux"
import { RouteComponentProps, withRouter } from "react-router-dom"
import { Actions } from "../../bussines/actions"
import { IStore } from "../../bussines/reducer"
import { useLocalization } from "../../context/localization"
import { useActions } from "../../data/hooks/useActions"
import { Button, Icon, Text } from "../_shared"
import styles from "./navigation.module.css"

type Props = RouteComponentProps<{ tab: string }> & {}

const Navigation: React.FC<Props> = ({ history, match: { params: { tab = "" } } }) => {




    const player1 = useSelector((s: IStore) => s.player1)
    const player2 = useSelector((s: IStore) => s.player2)
    const startGame = useSelector((s: IStore) => s.startGame)
    const actions = useActions(Actions, "setStartGame")

    const { translateText, language, changeLanguage } = useLocalization()


    const handleClick = (path: string) => history.push(path)
    const changeName = () => {
        actions.setStartGame(false)
    }

    const switchLang = React.useCallback(() => {
        if (language == "en")
            changeLanguage("ro")
        else
            changeLanguage("en")
    }, [language])

    return (
        <div className={styles.wrapper}>
            <Button
                className={styles.btn}
                onClick={() => handleClick("/")}
                isActive={tab != "results"}
                rounded> <Icon name="trello"></Icon>{translateText("home")}</Button>
            <Button
                className={styles.btn}
                onClick={() => handleClick("/results")}
                isActive={tab == "results"}
                rounded><Icon name="users"></Icon> {translateText("result")}</Button>


            <div className={styles.players}>
                <Button size="s" onClick={switchLang} rounded>{translateText("changelang")} </Button> <div></div>
                {startGame && <>
                    <Button size="s" className={styles.backBtn} onClick={changeName} skin="primary" icon="arrow-left">{translateText("changeNames")}</Button>
                    <Text>{player1} vs {player2}</Text>
                </>}
            </div>
        </div >
    )
}


export default withRouter(Navigation)