var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')

var resultRouter = require('./src/routes/results')
var cors = require('cors')
var app = express()

app.use(cors())
// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
const mongose = require('mongoose')

require('dotenv/config')


var options = {
  useNewUrlParser: true,
  server: {
    socketOptions: {
      keepAlive: 300000, connectTimeoutMS: 30000
    }
  },
  replset: {
    socketOptions: {
      keepAlive: 300000,
      connectTimeoutMS: 30000
    }
  }
}

mongose.connect(
  "mongodb+srv://narcisolimpiu:olimpiu@cluster0.2kas0.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", options, (err, client) => {
    console.log("connected")
  })

app.use(express.static('public'))

app.get('/results', function (req, res, next) {
  if (!req.url.includes("/api"))
    return res.sendfile('public/index.html')
  next()
})


app.use('/api/results', resultRouter)





module.exports = app
