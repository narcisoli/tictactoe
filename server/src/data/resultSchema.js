
const mongose = require("mongoose")


const ResultSchema = mongose.Schema({
    player1: {
        type: String,
        required: true
    },
    result: {
        type: Number,
        required: true
    },
    player2: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongose.model("Results", ResultSchema)