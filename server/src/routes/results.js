var express = require('express')
var router = express.Router()
var fs = require("fs")

const db = require("../data/resultSchema")


router.get('/', async (req, res, next) => {
    try {
        const data = await db.find()
        res.json(data)
    } catch (err) {
        res.status(500).json(err)
    }

     //asd
})


router.post('/add', async (req, res, next) => {
    const aux = new db({ ...req.body })

    try {
        const data = await aux.save()
        res.json(data)
    } catch (err) {
        res.status(500).json(err)
    }
})





module.exports = router
