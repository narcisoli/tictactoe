

module.exports = {
    plugins: [
        require('postcss-simple-vars'),
        require('autoprefixer'),
        require('postcss-nested'),
        require("postcss-bem"),
        require('@daltontan/postcss-import-json'),
    ]
}